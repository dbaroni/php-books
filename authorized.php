<?php
require_once "config.php";

if (($_SESSION['user']['role'] ?? '') != 'admin') {

    header('location: /admin/login.php');
    $_SESSION['backto'] = $_SERVER['REQUEST_URI'];
    die;

}