<?php 

$id = $_GET['id'] ?? 0;

require "../../config.php";

try {
    $stmt = $db->prepare("DELETE FROM authors WHERE id = ?");
    $stmt->bindParam(1, $id);
    $stmt->execute();
}
catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/authors/index.php')
?>