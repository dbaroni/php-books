<?php require_once "../../config.php"; ?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Elenco generi</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/admin.css">

    <ul class="navBar">
  <li class="navBarElement"><a href="../../index.php">Home</a></li>
  <li class="navBarElement"><a href="/admin/books/index.php">Libri</a></li>
  <li class="navBarElement"><a href="/admin/genres/index.php">Generi</a></li>
  <li class="navBarElement"><a href="/admin/authors/index.php">Autori</a></li>
  <div class="loginNavPos"> 
    <?php if(isset($_SESSION['user'])): ?>
        <li class="navBarElement"><a href="/admin/login_r.php">Logout</a></li>
    <?php else: ?>
        <li class="navBarElement"><a href="/admin/login.php">Login</a></li>
    <?php endif ?>
  </div>
</ul> 
</head>
<body>
<?php
require_once "../../authorized.php";

try {
    $stmt = $db -> prepare("
    SELECT * FROM authors
    ");
    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>
<h1>Elenco autori</h1>
<a href="add.php"><span class="material-icons">add_circle_outline</span></a>
<br><br>

<table>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>birth</th>
        <th>nationality</th>
        <th></th>
    </tr>
    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?= $row['id'] ?></td>
            <td><?= $row['name'] ?></td>
            <td><?= $row['birth'] ?></td>
            <td><?= $row['nationality'] ?></td>
            <td>
                <button onclick="mod(<?= $row['id'] ?>)"><span class="material-icons">edit</span></button>
                <button onclick="del(<?= $row['id'] ?>)"><span class="material-icons">delete</span></button>
            </td>

        </tr>
    <?php endwhile ?>
</table>

<br><br>

<script>
    function del(id) {
        if (confirm('Sei sicuro si voler eliminare questo autore?')) {
            location = "/admin/authors/del.php?id=" + id
        }
    }

    function mod(id) {
        location = "/admin/authors/edit.php?id=" + id;
    }
</script>

</body>
</html>