<?php

    require_once "../../config.php";
    require_once "../../authorized.php";
    
    #var_export($_POST); die;
    
    $name = $_POST['name'] ?? '';
    $birth = $_POST['birth'] ?? '';
    $nationality = $_POST['nationality'] ?? '';
    $id = intval($_POST['id'] ?? 0);
    
    if ($name == '' || $birth == null || $nationality == null) {
        # --> restituire messaggio di errore
        $_SESSION['add_data'] =  [
            'name' => $name,
            'birth' => $birth,
            'nationality' => $nationality
        ];
        header('location: /admin/authors/edit.php?id=' . $id);
        die;
    }
    
    try {
    
        $stmt = $db->prepare("UPDATE authors SET name = :name, birth = :birth, nationality = :nationality WHERE id = :id");
    
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':birth', $birth);
        $stmt->bindParam(':nationality', $nationality);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    
    
    } catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }
    
    header('location: /admin/authors/index.php');
    

    ?>
    
    
    
    