<?php

require_once "../../config.php";
require_once "../../authorized.php";

#var_export($_POST); die;

$name = $_POST['name'] ?? '';
$birth = $_POST['birth'] ?? 2000;
$nationality = $_POST['nationality'] ?? '';

try {
    $stmt = $db-> prepare("
    INSERT INTO authors SET 
    name = :name,
    birth = :birth,
    nationality = :nationality
    ");

    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':birth', $birth);
    $stmt->bindParam(':nationality', $nationality);
    $stmt->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/authors/index.php');

?>



