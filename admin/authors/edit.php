<?php require_once "../../config.php"; ?>
<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Modifica autore</title>
    <style>
        label {
            width: 4pc;
            display: inline-block;
        }

        input[value="Salva"] {
            background-color: yellowgreen;

        }
        
        input[type=submit], input[type=button], input[type=reset] {
            cursor: pointer;
            border: 1px solid #4444;
            border-radius: 2px;
        }
    </style>

<ul class="navBar">
  <li class="navBarElement"><a href="../../index.php">Home</a></li>
  <li class="navBarElement"><a href="/admin/books/index.php">Libri</a></li>
  <li class="navBarElement"><a href="/admin/genres/index.php">Generi</a></li>
  <li class="navBarElement"><a href="/admin/authors/index.php">Autori</a></li>
  <div class="loginNavPos"> 
    <?php if(isset($_SESSION['user'])): ?>
        <li class="navBarElement"><a href="/admin/login_r.php">Logout</a></li>
    <?php else: ?>
        <li class="navBarElement"><a href="/admin/login.php">Login</a></li>
    <?php endif ?>
  </div>
</ul> 
</head>
<body>

<?php
require_once "../../authorized.php";
$id = intval($_GET['id']) ?? 0;

try {

    $stmt = $db->prepare("SELECT * FROM authors WHERE id = :id");
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $authors = $stmt->fetch(PDO::FETCH_ASSOC);

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

if (isset($_SESSION['add_data'])) {
    $name = $_SESSION['add_data']['name'];
    $birth= $_SESSION['add_data']['birth'];
    $nationality= $_SESSION['add_data']['nationality'];
    unset($_SESSION['add_data']);
} else {
    $id = $authors['id'];
    $name = $authors['name'];
    $birth= $authors['birth'];
    $nationality= $authors['nationality'];
}
?>

<h2>Modifica authors</h2>

<br>

<form method="post" action="edit_r.php" enctype="multipart/form-data">

    <label for="name">name</label> <br>
    <input id="name" name="name" size="20" maxlength="15" value="<?= $name ?>"> <br><br>

    <label for="birth">birth</label> <br>
    <input id="birth" name="birth" size="50" maxlength="60" value="<?= $birth ?>"> <br><br>

    <label for="nationality">nationality</label> <br>
    <input id="nationality" name="nationality" size="50" maxlength="60" value="<?= $nationality ?>"> <br><br>

    <input hidden id="id" name="id" type="number" value="<?= $id ?>">

    <input type="button" value="Annulla" onclick="history.back()">
    <input type="reset">
    <input type="submit" value="Salva">


</form>

</body>
</html>