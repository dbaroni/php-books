<?php
require_once "../config.php";

//var_export($_POST);

$username = $_POST['username'] ?? '';
$password = $_POST['password'] ?? '';

unset($_SESSION['user']);

try {
    $stmt = $db->prepare(" 
    SELECT id, username, role FROM users
    WHERE username=:username AND password=MD5(CONCAT(:password, :salt))
    ");

    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':salt', $securitysalt);
    $stmt->execute();

    if ($user = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $_SESSION['user']['role'] = $user["role"];
        $_SESSION['msg'] = "Sei dentro";
    } else {
        $_SESSION['msg'] = "Non puoi accedere";
    }

} catch (PDOException $e) {
    echo $e->getMessage();
    die();
}

$backto = $_SESSION['backto'] ?? '/index.php';
unset($_SESSION['backto']);  //consumo il gettone

header("location: $backto");