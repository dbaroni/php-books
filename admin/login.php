<?php

require_once "../config.php";

if (!isset($_SESSION['backto'])) {

    $backto = $_SERVER['HTTP_REFERER'] ?? '';
    if ($backto == '') {
        $backto = '/index.php';
    }
    $_SESSION['backto'] = $backto;
}

?>
<h2> Login </h2>
<form method="post" action="login_r.php">
    username: <input name="username" size="30"><br><br>
    password: <input name="password" size="30"><br><br>
    <input type="submit" value="login now">
</form>