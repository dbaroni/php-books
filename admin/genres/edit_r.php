<?php
    require "../../config.php";

    $genre_id = intval($_POST['genre_id'] ?? 0);
    $genre = $_POST['genre'] ?? '';
    $description = $_POST['description'] ?? '';

    if($genre == '') $genre = null;
    if($description == '') $description = null;

    if ($genre == null || $description == null) {
        $_SESSION['genre_add_data'] = [
            'msg' => 'Alcuni dati sono mancanti',
            'genre' => $genre,
            'description' => $description,
        ];
        header('location: /admin/genres/edit.php?id=' .$id);
        die;
    }

    try {
        $stmt = $db->prepare("UPDATE genres SET genre = :genre, description = :description WHERE id= :id");
        $stmt->bindParam(':genre', $genre);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':id', $genre_id);
        $stmt->execute();
    }
    catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }
    header('location: /admin/genres/index.php')
?>