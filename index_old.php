<?php require "config.php"; ?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Elenco libri</title>
    <link rel="stylesheet" href="assets/public.css">

<ul class="navBar">
  <li class="navBarElement"><a href="index.php">Home</a></li>
    <li class="navBarElement"><a href="/admin/books/index.php">Libri</a></li>
    <li class="navBarElement"><a href="/admin/genres/index.php">Generi</a></li>
    <li class="navBarElement"><a href="/admin/authors/index.php">Autori</a></li>

  <div class="loginNavPos"> 

    <?php if(isset($_SESSION['user'])): ?>
        <li class="navBarElement"><a href="/admin/login_r.php">Logout</a></li>
    <?php else: ?>
        <li class="navBarElement"><a href="/admin/login.php">Login</a></li>
    <?php endif ?>

  </div>
</ul> 
</head>
<body>
<?php

$pag = intval($_GET['pag'] ?? '0');
$NRP = 5;
$offset = $pag * $NRP;

$q = $_GET['q'] ?? '';
$order = $_GET['order'] ?? '';
if (!in_array($order, ['','title','authors','genre','year','price'])) {
    $order= '';
}


$sql = "
    SELECT SQL_CALC_FOUND_ROWS 
    B.id, B.title, B.year, B.price, G.genre,
        GROUP_CONCAT(A.name SEPARATOR ', ') AS authors
    FROM books B
        LEFT JOIN genres G ON B.genre_id = G.id
        LEFT JOIN authors_books AB ON B.id = AB.book_id
        LEFT JOIN authors A ON AB.author_id = A.id ";

$sql .= "GROUP BY B.id ";
if ($q != '') {
    $sql .= "
    HAVING B.title LIKE :q 
    OR authors LIKE :q 
    OR genre LIKE :q 
    " ;
}
if ($order != '') {
    $sql .= "
    ORDER BY $order ASC
    " ;
}

$sql .= " LIMIT $offset, $NRP";

try {
    $stmt = $db-> prepare($sql);
    if ($q != '') {
        $stmt->bindValue(':q', "%$q%");
    }
    $stmt->execute();
    //$stmt->debugDumpParams();

    $res = $db->query("SELECT FOUND_ROWS() AS TREC");
    $TREC = intval($res->fetch()['TREC']);
    //var_export($TREC);

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>

<h1>La Biblioteca</h1>

<form id="qform">
    <label for="q"> cerca per <input name="q" id="q" value="<?php $q ?>"></label>
    <button> Q </button>
    <button onclick="this.form.q.value = ''">X</button>
    <input type="hidden" name="order" value="<?php $order ?>">
    <input type="hidden" name="pag" value="<?php $pag ?>">
</form>
<br>

<table>
    <tr>
        <th>titolo<button onclick="sortBy('title')">^</button></th>
        <th>autori<button onclick="sortBy('authors')">^</button></th>
        <th>generi<button onclick="sortBy('genre')">^</button></th>
        <th>anno<button onclick="sortBy('year')">^</button></th>
        <th>prezzo<button onclick="sortBy('price')">^</button></th>
    </tr>
    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?= $row['title'] ?></td>
            <td><?= $row['authors'] ?></td>
            <td><?= $row['genre'] ?></td>
            <td><?= $row['year'] ?></td>
            <td><?= $row['price'] ?></td>
        </tr>
    <?php endwhile ?>
    <tr>
        <td colspan="99" style="text-align: center">
            <button onclick="goto(0)">|&lt;</button>
            <button onclick="go(-1)">&lt;</button>
            <button onclick="go(1)">&gt;</button>
            <button onclick="goto(<?= intval($TREC / $NRP) ?>)">&gt;|</button>

        </td>
    </tr>
</table>



<script>
    function sortBy(field) {
        document.getElementById("qform").order.value = field;
        document.getElementById("qform").submit();
    }

    function goto(pag) {
    document.getElementById("qform").pag.value = pag;
    document.getElementById("qform").submit();
    }

    function go(diquant) {
        let LASTPAGE = <?= ceil($TREC / $NRP) - 1 ?>;
        let p = diquant + 1*document.getElementById("qform").pag.value;
        if (p < 0) p = 0;
        if (p > LASTPAGE) p = LASTPAGE
        document.getElementById("qform").pag.value = p;
        document.getElementById("qform").submit();
    }
</script>
</body>
</html>