<?php

require "config.php";

$pag = max(0, intval($_GET['pag'] ?? '0'));
$NRP = 5;
$offset = $pag * $NRP;

$q = $_GET['q'] ?? '';
$order = $_GET['order'] ?? '';
if (!in_array($order, ['','title','authors','genre','year','price'])) {
    $order= '';
}


$sql = "
    SELECT SQL_CALC_FOUND_ROWS 
    B.id, B.title, B.year, B.price, G.genre,
        GROUP_CONCAT(A.name SEPARATOR ', ') AS authors
    FROM books B
        LEFT JOIN genres G ON B.genre_id = G.id
        LEFT JOIN authors_books AB ON B.id = AB.book_id
        LEFT JOIN authors A ON AB.author_id = A.id ";

$sql .= "GROUP BY B.id ";
if ($q != '') {
    $sql .= "
    HAVING B.title LIKE :q 
    OR authors LIKE :q 
    OR genre LIKE :q 
    " ;
}
if ($order != '') {
    $sql .= "
    ORDER BY $order ASC
    " ;
}

$sql .= " LIMIT $offset, $NRP";

try {
    $stmt = $db-> prepare($sql);
    if ($q != '') {
        $stmt->bindValue(':q', "%$q%");
    }
    $stmt->execute();
    //$stmt->debugDumpParams();

    $res = $db->query("SELECT FOUND_ROWS() AS TREC");
    $TREC = intval($res->fetch()['TREC']);
    //var_export($TREC);

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>

    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?= $row['title'] ?></td>
            <td><?= $row['authors'] ?></td>
            <td><?= $row['genre'] ?></td>
            <td><?= $row['year'] ?></td>
            <td><?= $row['price'] ?></td>
        </tr>
    <?php endwhile ?>
    <tr>
        <td colspan="99" style="text-align: center">
            <button onclick="goto(0)">|&lt;</button>
            <button onclick="go(-1)">&lt;</button>
            <button onclick="go(1)">&gt;</button>
            <button onclick="goto(<?= max(0, ceil($TREC / $NRP) - 1) ?>)">&gt;|</button>

        </td>
    </tr>

<input type="hidden" id="LASTPAGE" value="<?= max(0, ceil($TREC / $NRP) - 1) ?>">