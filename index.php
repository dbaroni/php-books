<?php require "config.php"; ?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Elenco libri</title>
    <link rel="stylesheet" href="assets/public.css">

<ul class="navBar">
  <li class="navBarElement"><a href="index.php">Home</a></li>
    <li class="navBarElement"><a href="/admin/books/index.php">Libri</a></li>
    <li class="navBarElement"><a href="/admin/genres/index.php">Generi</a></li>
    <li class="navBarElement"><a href="/admin/authors/index.php">Autori</a></li>

  <div class="loginNavPos"> 

    <?php if(isset($_SESSION['user'])): ?>
        <li class="navBarElement"><a href="/admin/login_r.php">Logout</a></li>
    <?php else: ?>
        <li class="navBarElement"><a href="/admin/login.php">Login</a></li>
    <?php endif ?>

  </div>
</ul> 
</head>
<body>

<?php

?>

<h1>La Biblioteca</h1>

<form method="get" id="qform" onsubmit="querybooks(this); return false;">
    <label for="q"> cerca per <input name="q" id="q" value=""
        onchange="this.form.pag.value=0"
        ></label>
    <button> Q </button>
    <button onclick="this.form.q.value = ''; this.form.pag.value=0">X</button>
    <input type="hidden" name="order" value="">
    <input type="hidden" name="pag" value="">
</form>
<br>

<table>
    <tr>
        <th>titolo<button onclick="sortBy('title')">^</button></th>
        <th>autori<button onclick="sortBy('authors')">^</button></th>
        <th>generi<button onclick="sortBy('genre')">^</button></th>
        <th>anno<button onclick="sortBy('year')">^</button></th>
        <th>prezzo<button onclick="sortBy('price')">^</button></th>
    </tr>
    <tbody id="tablebody"></tbody>
</table>


<script>

    function querybooks(form) {
        let url = `books.php?q=${form.q.value}&pag=${form.pag.value}&order=${form.order.value}`;
        ajaxLoad(url, (text) => {
            let script = text.search(/<script>(.*?)<\/script>/);
            eval(script);
            document.getElementById('tablebody').innerHTML = text;
        });
    }

    function sortBy(field) {
        document.getElementById("qform").order.value = field;
        //document.getElementById("qform").submit();
        querybooks(document.getElementById("qform"));
    }

    function goto(pag) {
    document.getElementById("qform").pag.value = pag;
        //document.getElementById("qform").submit();
        querybooks(document.getElementById("qform"));
    }

    var LASTPAGE = 0;
    function lastpage() {
        return parseInt(document.getElementById("LASTPAGE").value);
    }

    function go(diquant) {

        let p = diquant + 1*document.getElementById("qform").pag.value;
        if (p < 0) p = 0;
        if (p > lastpage()) p = lastpage()
        document.getElementById("qform").pag.value = p;
        //document.getElementById("qform").submit();
        querybooks(document.getElementById("qform"));
    }

    window.onload = function () {
        ajaxLoad('books.php', (text) => {
            document.getElementById('tablebody').innerHTML = text;
        });
    }

    function ajaxLoad(url, callback, method='GET') {
        fetch(url)
            .then(response => response.text())
            .then(data => callback(data))
        /*
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            callback(this.responseText);
        }
        xhttp.open(method, url, true);
        xhttp.send();

        */
    }


</script>
</body>
</html>